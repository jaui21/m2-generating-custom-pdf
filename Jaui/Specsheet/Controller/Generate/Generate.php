<?php
namespace Jaui\Specsheet\Controller\Generate;

use Magento\Framework\App\Action\Action;

class Generate extends Action
{
   /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $productCollectionFactory;
    protected $productRepositoryFactory;   
    protected $fileFactory;
 
    /**
     * @param Context                               $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory,        
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory) 
    {
        $this->productRepositoryFactory = $productRepositoryFactory;
        $this->productCollectionFactory = $productCollectionFactory;    	
        $this->fileFactory = $fileFactory;
        parent::__construct($context);
    }
 
    /**
     * to generate pdf
     *
     * @return void
     */
    public function execute()
    {
        // $productId = $this->getRequest()->getParams('pid');
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToSelect('*');
        // $productCollection->addIdFilter($productId);

          //*****************************
         //  Query direct to database 
        //*****************************
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        foreach ($productCollection as $product) { 

        $select = $connection->select()
            ->from('tm_brand','logo')
            ->where('brand_id =?', $product->getData('brand_id'));
        $brand_file = $connection->fetchOne($select);

        $pdf = new \Zend_Pdf();
        $pdf->pages[] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $page = $pdf->pages[0]; // this will get reference to the first page.
        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Rgb(0,0,0));
        $font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontbold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $style->setFont($font,15);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $x = 30;
        $pageTopalign = 850; //default PDF page height
        $this->y = 850 - 100; //print table row from page top – 100px

        $rootPath = "/home/webuser/public_dev";

        // Product Specification Sheet    
		$orangeLine =  \Zend_Pdf_Image::imageWithPath($rootPath . "/pub/specsheet/images/product-specification-sheet.png" );
        $page->drawImage($orangeLine, 30 , -100 , 70, 700);

        // Product Brand Logo
        $brandImagefile = $rootPath . "/pub/media/brandproductpage/brandproductpage/resized/250/" . $brand_file;
        if(file_exists($brandImagefile)):    
        $brandImage =  \Zend_Pdf_Image::imageWithPath($brandImagefile);
        $page->drawImage($brandImage, $x + 70 , $this->y-270, 250, $this->y -120);
        endif;

        // Product Name    
        $style->setFont($font,18);
        $page->setStyle($style);
        $page->drawText(__("%1", $product->getName()), $x + 70, $this->y - 250 , 'UTF-8');

        // Product Code
        $style->setFont($fontbold,12);
        $page->setStyle($style);        
        $page->drawText(__("Product Code : %1", $product->getSku()), $x + 70, $this->y -270, 'UTF-8');
 
        // Product Image
        $productImagefile = $rootPath . "/pub/media/catalog/product/" . $product->getImage();

        if(file_exists($productImagefile)):
            $productImage =  \Zend_Pdf_Image::imageWithPath($productImagefile);
            list($width, $height) = getimagesize($productImagefile);
            if($height > $width) {
                $page->drawImage($productImage, 110 , $this->y -600, 330, $this->y - 300); // portrait orientation
            } else {
                $page->drawImage($productImage, 80 , $this->y -540, 390, $this->y - 320);  // landscape orientation
            }
        endif;

        // Product Dimension    
        $style->setFont($fontbold,12);
        $page->setStyle($style); 
        $page->drawText(__("Dimension:"), $x + 370, $this->y-340, 'UTF-8');
        $style->setFont($font,11);
        $page->setStyle($style);
        $dimension = $this->getDimension($product->getId());
        $page->drawText(__("%1",  $dimension), $x + 370, $this->y-360, 'UTF-8');

        // Product Warranty    
        $style->setFont($fontbold,12);
        $page->setStyle($style); 
        $page->drawText(__("Warranty:"), $x + 370, $this->y-380, 'UTF-8');
        $style->setFont($font,11);
        $page->setStyle($style); 
        $warranty =  $this->getWarranty($product->getId()); 
        $warrantylabel =  $this->getWarrantylabel($product->getId(), $warranty);
        if(!empty($warranty)):    
        $page->drawText(__("%1", $warrantylabel), $x + 370, $this->y-400, 'UTF-8');
        endif;

        // Product Features
        $style->setFont($fontbold,12);
        $page->setStyle($style); 
        $page->drawText(__("Features:"), $x + 370, $this->y-420, 'UTF-8');
        $featureY = $this->y - 440;
        $style->setFont($font,11);
        $page->setStyle($style);        
        $description = wordwrap($product->getDescription(), 90, "</li>");
        
        $arrdescription = explode("</li>", $description); 
        $count = count($arrdescription);
        foreach (explode("</li>", $description) as $i => $line) {

            if (--$count <= 0) { break; }    
            $page->drawText(__("- %1", strip_tags($line)) , $x + 370, $featureY - $i * 18, 'UTF-8');
        }

        // Black Rectangle   
        $style->setFillColor(new \Zend_Pdf_Color_Html('#BBBBBB'));
        $page->setStyle($style);
        $page->drawRectangle(0, $this->y -690, 29, $this->y - 750, \Zend_Pdf_Page::SHAPE_DRAW_FILL);
        $page->drawRectangle(71, $this->y -690, $page->getWidth(), $this->y - 750, \Zend_Pdf_Page::SHAPE_DRAW_FILL);

        // Footer Note
        $style->setFont($font,8);
        $page->setStyle($style);
        $page->drawText(__("NOTE: Images, specifications and dimension correct at the time of publishing. It is advised that these details be"), 220, $this->y-670);
        $page->drawText(__("confirmed prior to construction as no liability will be taken by Harvey Norman Commercial for any changes in specifications."), 200, $this->y-680);

        // Harvey Norman Logo    
        $imagehncffe =  \Zend_Pdf_Image::imageWithPath($rootPath . "/pub/specsheet/images/harveynorman-ffe-white.png");
        $page->drawImage($imagehncffe, 350 , $this->y -733, 500, $this->y - 703);


        $fileName = $product->getSku() . '.pdf';
        //$fileName = 'example.pdf';    

        $pdf->save("/home/webuser/public_dev/pub/specsheet/" .  $fileName );

        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $this->_redirect( $baseUrl . 'specsheet/' . $fileName);
    	}   

    }
    public function getDimension($id) 
    {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($id);

            return $_product->getDimension();
    }
    public function getWarranty($id) 
    {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($id);

            return $_product->getWarranty();
    } 
    public function getWarrantylabel($id, $optionid) 
    {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($id);

            $attr = $_product->getResource()->getAttribute('warranty');
             if ($attr->usesSource()) {
                   $optionText = $attr->getSource()->getOptionText($optionid);
             }
            return $optionText;
    }                
}